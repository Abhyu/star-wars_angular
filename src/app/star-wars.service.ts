import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Jsonp} from '@angular/http';

import { LogService } from './log.service';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';


@Injectable()
export class StarWarsService {
  private characters = [];
  private logService: LogService;
  charactersChanged = new Subject<void>();
  private _URL = '/api/api/1.0/calendar?callback=JSONP_CALLBACK&dp_auth=NXs7tKEBsK543sxF' +
  '&period=DAILY&dt=20180708&tz=Asia/Kolkata&u=livesquawk&s=ecal.ecopulse.watch&rl=all&l=en';

  public data: string[];
  constructor(logService: LogService, private http: HttpClient, private jsonp: Jsonp) {
    this.logService = logService;
  }

  // this runs asynchronous
  fetchCharacter() {
    this.jsonp.request(this._URL)
    .map((res) => {
      const data = res._body.data;
      const indi = data.map((ind) => {
          return {name: ind.icode + ' ' + ind.tc, side: '' };
      });
      console.log(res._body.data);
      return indi;
    })
      .subscribe(
        (data) => {
          // console.log(data);
          this.characters = data;
          this.charactersChanged.next();
        }
      );
  }

  getCharacters(chosenList) {
    if (chosenList === 'all') {
      return this.characters.slice();
    }
    return this.characters.filter((char) => {
      return char.side === chosenList;
    });
  }

  onSideChosen(charInfo) {
    const pos = this.characters.findIndex((char) => {
      return char.name === charInfo.name;
    });
    this.characters[pos].side = charInfo.side;
    this.charactersChanged.next();
    this.logService.writeLog('Changed side of ' + charInfo.name + ', new side: ' + charInfo.side);
  }

  addCharacter(name, side) {
    const pos = this.characters.findIndex((char) => {
      return char.name === name;
    });
    if (pos !== -1) {
      return;
    }
    const newChar = {name: name, side: side};
    this.characters.push(newChar);
  }
}
